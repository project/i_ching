<?php
  drupal_add_css(drupal_get_path('module', 'i_ching') .'/iching.css'); 
  drupal_add_css(drupal_get_path('module', 'i_ching') .'/thickbox/thickbox.css');
  drupal_add_js(drupal_get_path('module', 'i_ching') .'/thickbox/thickbox.js');
?>

<script>
  $(document).ready(function(){
    $(".thickbox").each( function() {
      $(this).attr('href',$(this).attr('href') + '/popup');
    });
  });
</script>

<div id="iching-chart-wrap">

<table>

<tr>
<th></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-1.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-2.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-3.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-4.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-5.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-6.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-7.gif'; ?>"></th>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-8.gif'; ?>"></th>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-1.gif'; ?>"></th>
<td><?php print l('1', 'iching/hexagram-chart/hexagram/1', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('34'), 'iching/hexagram-chart/hexagram/34', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('5'), 'iching/hexagram-chart/hexagram/5', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('26'), 'iching/hexagram-chart/hexagram/26', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('11'), 'iching/hexagram-chart/hexagram/11', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('9'), 'iching/hexagram-chart/hexagram/9', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('14'), 'iching/hexagram-chart/hexagram/14', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('43'), 'iching/hexagram-chart/hexagram/43', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-2.gif'; ?>"></th>
<td><?php print l(t('25'), 'iching/hexagram-chart/hexagram/25', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('51'), 'iching/hexagram-chart/hexagram/51', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('3'), 'iching/hexagram-chart/hexagram/3', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('27'), 'iching/hexagram-chart/hexagram/27', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('24'), 'iching/hexagram-chart/hexagram/24', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('42'), 'iching/hexagram-chart/hexagram/42', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('21'), 'iching/hexagram-chart/hexagram/21', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('17'), 'iching/hexagram-chart/hexagram/17', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-3.gif'; ?>"></th>
<td><?php print l(t('6'), 'iching/hexagram-chart/hexagram/6', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('40'), 'iching/hexagram-chart/hexagram/40', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('29'), 'iching/hexagram-chart/hexagram/29', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('4'), 'iching/hexagram-chart/hexagram/4', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('7'), 'iching/hexagram-chart/hexagram/7', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('59'), 'iching/hexagram-chart/hexagram/59', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('64'), 'iching/hexagram-chart/hexagram/64', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('47'), 'iching/hexagram-chart/hexagram/47', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-4.gif'; ?>"></th>
<td><?php print l(t('33'), 'iching/hexagram-chart/hexagram/33', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('62'), 'iching/hexagram-chart/hexagram/62', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('39'), 'iching/hexagram-chart/hexagram/39', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('52'), 'iching/hexagram-chart/hexagram/52', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('15'), 'iching/hexagram-chart/hexagram/15', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('53'), 'iching/hexagram-chart/hexagram/53', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('56'), 'iching/hexagram-chart/hexagram/56', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('31'), 'iching/hexagram-chart/hexagram/31', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-5.gif'; ?>"></th>
<td><?php print l(t('12'), 'iching/hexagram-chart/hexagram/12', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('16'), 'iching/hexagram-chart/hexagram/16', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('8'), 'iching/hexagram-chart/hexagram/8', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('23'), 'iching/hexagram-chart/hexagram/23', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('2'), 'iching/hexagram-chart/hexagram/2', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('20'), 'iching/hexagram-chart/hexagram/20', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('35'), 'iching/hexagram-chart/hexagram/35', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('45'), 'iching/hexagram-chart/hexagram/45', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-6.gif'; ?>"></th>
<td><?php print l(t('44'), 'iching/hexagram-chart/hexagram/44', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('32'), 'iching/hexagram-chart/hexagram/32', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('48'), 'iching/hexagram-chart/hexagram/48', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('18'), 'iching/hexagram-chart/hexagram/18', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('46'), 'iching/hexagram-chart/hexagram/46', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('57'), 'iching/hexagram-chart/hexagram/57', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('50'), 'iching/hexagram-chart/hexagram/50', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('28'), 'iching/hexagram-chart/hexagram/28', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-7.gif'; ?>"></th>
<td><?php print l(t('13'), 'iching/hexagram-chart/hexagram/13', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('55'), 'iching/hexagram-chart/hexagram/55', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('63'), 'iching/hexagram-chart/hexagram/63', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('22'), 'iching/hexagram-chart/hexagram/22', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('36'), 'iching/hexagram-chart/hexagram/36', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('37'), 'iching/hexagram-chart/hexagram/37', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('30'), 'iching/hexagram-chart/hexagram/30', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('49'), 'iching/hexagram-chart/hexagram/49', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

<tr>
<th><img src="<?php echo '/'. drupal_get_path('module', 'i_ching') .'/images/triagram-8.gif'; ?>"></th>
<td><?php print l(t('10'), 'iching/hexagram-chart/hexagram/10', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('54'), 'iching/hexagram-chart/hexagram/54', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('60'), 'iching/hexagram-chart/hexagram/60', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('41'), 'iching/hexagram-chart/hexagram/41', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('19'), 'iching/hexagram-chart/hexagram/19', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('61'), 'iching/hexagram-chart/hexagram/61', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('38'), 'iching/hexagram-chart/hexagram/38', array('attributes' => array('class' => 'thickbox'))); ?></td>
<td><?php print l(t('58'), 'iching/hexagram-chart/hexagram/58', array('attributes' => array('class' => 'thickbox'))); ?></td>
</tr>

</table>
</div>