if (Drupal.jsEnabled) {
      $(document).ready(function() {
            if (varline_1 == 3 || varline_1 == 6) {
                  $("#primary-text .line-number-1").css("color", "blue");
                  }
            if (varline_2 == 3 || varline_2 == 6) {
                  $("#primary-text .line-number-2").css("color", "blue");
                  // Below is concatenated example for adding variable to selector statement
                  //  $("#primary-text[class="+ this_nid +"] .line-number-2").css("color", "blue");
                  // Here's another method to concatenate
                  //  $('.orderInfo#'+tabText); 
                  }
            if (varline_3 == 3 || varline_3 == 6) {
                  $("#primary-text .line-number-3").css("color", "blue");
                  }
            if (varline_4 == 3 || varline_4 == 6) {
                  $("#primary-text .line-number-4").css("color", "blue");
                  }
            if (varline_5 == 3 || varline_5 == 6) {
                  $("#primary-text .line-number-5").css("color", "blue");
                  }
            if (varline_6 == 3 || varline_6 == 6) {
                  $("#primary-text .line-number-6").css("color", "blue");
                  }
      });
}