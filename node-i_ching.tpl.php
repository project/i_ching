<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if ($status = 0) { print ' node-unpublished'; } ?>">

  <?php print $picture ?>

  <?php if ($page == 0): ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <?php endif; ?>

  <div class="content clear-block">
    <?php print $content ?>

    <?php
      // Highlight changing lines for single view only. May make work for multi-item view in future
      if ($page != 0) {
        highlight_changing_lines($node);
      }
      $output = '<div class="i_ching_order_info">';
        if ($secondary_object['convert_secondary_lines_step_2'] != $primary_object['convert_primary_lines_step_2']) {
        }
        elseif ($secondary_object['convert_secondary_lines_step_2'] == $primary_object['convert_primary_lines_step_2']) {
          $output .= "<p>No secondary hexagram, because there are no changing lines.</p>";
        }
        // Output primary image and text
        $output .= '<fieldset class="collapsible collapsed"><p><legend>Primary hexagram: #'. $primary_object['primary_hexagram']['number'] .', '. $primary_object['primary_hexagram']['name'] .'</legend></p><img src="/'. $primary_hex_image_name .'" alt="" style="float:left;" /><span id="primary-text"><p>'. $primary_hex_text .'</p></span></fieldset>';

        // Begin secondary hexagram text and image
        if ($secondary_object['convert_secondary_lines_step_2'] != $primary_object['convert_primary_lines_step_2']) {
        // Output secondary image and text
        $output .= '<fieldset class="collapsible collapsed"><p><legend>Secondary hexagram: #'. $secondary_object['secondary_hexagram']['number'] .', '. $secondary_object['secondary_hexagram']['2ndhexname'] .'</legend></p><img src="/'. $secondary_hex_image_name .'" alt="" style="float:left;" /><p>'. $secondary_hex_text .'</p></fieldset>';
        }
      $output .= '</div><!-- /.i_ching_order_info -->';
      print $output;
    ?>
    
  </div><!-- /.content -->
  <div class="clear-block">
    <div class="meta">
      <?php if ($taxonomy): ?>
        <div class="terms"><?php print $terms ?></div><!-- /.term -->
      <?php endif;?>
    </div><!-- /.meta -->

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div><!-- /.links -->
    <?php endif; ?>
  </div><!-- /.clear-block -->
</div><!-- /#node -->